$.getJSON("data.json", function(data) {
  var questionIndex = 0;
  populateQA();

  function populateQA() {
    $("#question").text(
      questionIndex + 1 + "/" + data.length + " " + data[questionIndex].question
    );
    for (i = 0; i < data[questionIndex].options.length; i++) {
      $(".answer:eq(" + i + ")").text(data[questionIndex].options[i]);
    }
  }
  var correctAnswers = 0;
  var lastAnswer = null;
  var listAnswer = [];

  $(".answer").click(function(evt) {
    lastAnswer = $(evt.target).text();
    $(".answer").removeClass("choice");
    $(evt.target).addClass("choice");
    $("#next").removeClass("invisible");
  });

  $("#next").click(function() {
    listAnswer.push(lastAnswer);
    if (lastAnswer == data[questionIndex].answer) correctAnswers += 1;
    questionIndex;
    if (questionIndex == data.length - 2) {
      document.getElementById("next").innerHTML = "Enviar respostas";
    }
    $(this).addClass("invisible");
    $(".answer").removeClass("choice");
    questionIndex += 1;
    questionIndex >= data.length ? getScore() : populateQA();
    $("#wrapper").animate({ width: "toggle" }, 350);
    $("#wrapper").animate({ width: "toggle" }, 350);
  });
  function getScore() {
    var text = "";
    for (var i = 0; i < data.length; i++) {
      k = i + 1;
      text += k + "/" + data.length + " " + data[i].question + "<br>";
      for (j = 0; j < data[i].options.length; j++) {
        data[i].options[j] == data[i].answer
          ? (text += "<strong>" + data[i].options[j] + "</strong>")
          : (text += data[i].options[j]);
        if (data[i].options[j] == listAnswer[i]) {
          listAnswer[i] == data[i].answer
            ? (text += "<span>&#10004;</span>")
            : (text += "<span>&#10005;</span>");
        }
        text += "<br>";
      }
      text += "<br>";
    }
    var answer =
      correctAnswers == 1 ? "resposta correta" : "respostas corretas";
    $("#results")
      .removeClass("invisible")
      .addClass("visible")
      .text("Oi");
    $("#wrapper")
      .children()
      .remove();
    $("#wrapper")
      .removeClass("answers")
      .addClass("result")
      .append(
        "<div class='score'>Você obteve " +
          correctAnswers +
          " " +
          answer +
          " de " +
          data.length +
          " questões.</div>" +
          "<br>" +
          "<div class='results'>" +
          text +
          "</div>"
      );
  }
});
